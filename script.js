var fs = require("fs");

function readWriteSync() {
  console.log("[readWriteSync] Start sync environment.");

  // read write environment
  var envfile = fs.readFileSync(
    `src/env/${process.env.NODE_ENV.trim()}.env`,
    "utf-8"
  );
  fs.writeFileSync("./.env", envfile, "utf-8");
  console.log(
    `=== Generated file .env for prisma with NODE_ENV = ${process.env.NODE_ENV.trim()} ===`
  );
}

readWriteSync();
