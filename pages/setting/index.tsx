import React, { ReactElement } from "react";
import DashboardLayout from "../../src/components/Layout";
import Container from "../../src/components/Container";
import { Divider, Input, List, PageHeader, Switch } from "antd";
import { Context } from "../../src/lib/context";

interface Props {}

enum TypeSetting {
  VALUE = "VALUE",
  SWITCH = "SWITCH",
}

enum TypeUnit {
  TIME = "TIME",
  MILLISECOND = "MILLISECOND",
  MINUTES = "MINUTES",
  HOURS = "HOURS",
}

interface SettingObject {
  type: TypeSetting;
  default: any;
  value: any;
  unit?: TypeUnit;
}

interface DataSetting {
  key: string;
  title: string;
  description?: string;
  setting: SettingObject;
}

export default function Setting(props): ReactElement {
  const onToggle = (checked) => {
    console.log(checked);
  };

  const { state } = React.useContext(Context);
  console.log("SETTING PAGE");
  console.log("state : ", state);

  const aocDataSetting: DataSetting[] = [
    {
      key: "aoc-true-security",
      title: "TRUE-H Security",
      description:
        "Check user behavior according to the conditions of TRUE-H Operator",
      setting: {
        type: TypeSetting.SWITCH,
        default: true,
        value: true,
      },
    },
    {
      key: "aoc-timeallow-sub-min",
      title: "Subscription Button Click Time (Min)",
      description: "At least acceptable time (before pressing subscription)",
      setting: {
        type: TypeSetting.VALUE,
        default: 0,
        value: 0,
        unit: TypeUnit.MILLISECOND,
      },
    },
    {
      key: "aoc-timeallow-sub-max",
      title: "Subscription Button Click Time (Max)",
      description: "The most acceptable time (before pressing subscription) ",
      setting: {
        type: TypeSetting.VALUE,
        default: 6000,
        value: 6000,
        unit: TypeUnit.MILLISECOND,
      },
    },
    {
      key: "aoc-true-mismatch",
      title: "TRUE-H Mismatch Checker",
      description: "Check mismatch with TRUE-H Operator",
      setting: {
        type: TypeSetting.SWITCH,
        default: true,
        value: true,
      },
    },
    {
      key: "aoc-check-msisdn-sub-per-ip",
      title: "Check MSISDN with IP Address",
      description: "Check MSISDN with IP Address",
      setting: {
        type: TypeSetting.SWITCH,
        default: false,
        value: false,
      },
    },
    {
      key: "aoc-timeallow-sub-per-ip",
      title: "Time allowed to subscrition per 1 IP Address",
      description: "The most acceptable time (before pressing subscription) ",
      setting: {
        type: TypeSetting.VALUE,
        default: 10000,
        value: 10000,
        unit: TypeUnit.MILLISECOND,
      },
    },
    {
      key: "aoc-time-block",
      title: "ฺBlock Time",
      description: "Prohibition period to enter AOC landing page",
      setting: {
        type: TypeSetting.VALUE,
        default: 60,
        value: 60,
        unit: TypeUnit.MINUTES,
      },
    },
  ];

  const quizDataSetting: DataSetting[] = [
    {
      key: "quiz-limit-banner",
      title: "Limit Banner",
      description: "Limit the number of banner images on Quiz landing page",
      setting: {
        type: TypeSetting.VALUE,
        default: 5,
        value: 5,
      },
    },
  ];

  return (
    <Container>
      <Container.Body>
        <DashboardLayout defaultSelectedKeys="3">
          {/* <PageHeader
            style={{
              background: "white",
              borderBottom: "1px solid #f2f2f2",
            }}
            ghost={true}
            title="Setting"
          /> */}
          <div style={{ padding: 16 }}>
            <Divider orientation="left">
              <h3>AOC Setting</h3>
            </Divider>
            <List
              itemLayout="horizontal"
              dataSource={aocDataSetting}
              renderItem={(item) => (
                <List.Item
                  actions={[
                    item.setting.type === TypeSetting.SWITCH ? (
                      <Switch
                        defaultChecked={item.setting.value}
                        //onChange={onToggle}
                        //checked={item.setting.value}
                      />
                    ) : (
                      <div
                        style={{
                          display: "flex",
                          padding: "16px 0px 8px 0px",
                          flexDirection: "column",
                          alignItems: "flex-end",
                        }}
                      >
                        <Input
                          style={{ maxWidth: "160px" }}
                          suffix={item.setting.unit}
                          value={item.setting.value}
                        />
                        <p style={{ paddingTop: 6, fontSize: "10px" }}>
                          Default : {item.setting.default}
                        </p>
                      </div>
                    ),
                  ]}
                >
                  <List.Item.Meta
                    title={item.title}
                    description={item.description}
                  />
                </List.Item>
              )}
            />

            <Divider orientation="left">
              <h3>Quiz Setting</h3>
            </Divider>
            <List
              itemLayout="horizontal"
              dataSource={quizDataSetting}
              renderItem={(item) => (
                <List.Item
                  actions={[
                    item.setting.type === TypeSetting.SWITCH ? (
                      <Switch
                        defaultChecked={item.setting.value}
                        //onChange={onToggle}
                        //checked={item.setting.value}
                      />
                    ) : (
                      <div
                        style={{
                          display: "flex",
                          padding: "16px 0px 8px 0px",
                          flexDirection: "column",
                          alignItems: "flex-end",
                        }}
                      >
                        <Input
                          style={{ width: "60%" }}
                          suffix={item.setting.unit}
                          value={item.setting.value}
                        />
                        <p style={{ paddingTop: 6, fontSize: "10px" }}>
                          Default : {item.setting.default}
                        </p>
                      </div>
                    ),
                  ]}
                >
                  <List.Item.Meta
                    title={item.title}
                    description={item.description}
                  />
                </List.Item>
              )}
            />
          </div>
        </DashboardLayout>
      </Container.Body>
    </Container>
  );
}
