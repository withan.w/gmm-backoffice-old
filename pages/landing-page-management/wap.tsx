import React, { ReactElement, useState, useEffect } from "react";
import DashboardLayout from "../../src/components/Layout";
import Container from "../../src/components/Container";

interface Props {}

export default function WapManagement(props): ReactElement {
  return (
    <Container>
      <Container.Body>
        <DashboardLayout defaultSelectedKeys="2-2">
          <h3>WAP Management</h3>
        </DashboardLayout>
      </Container.Body>
    </Container>
  );
}
