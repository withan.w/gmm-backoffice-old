import React, { ReactElement, useState, useEffect } from "react";
import DashboardLayout from "../../src/components/Layout";
import Container from "../../src/components/Container";

interface Props {}

export default function QuizManagement(props): ReactElement {
  return (
    <Container>
      <Container.Body>
        <DashboardLayout defaultSelectedKeys="2-5">
          <h3>Quiz Management</h3>
        </DashboardLayout>
      </Container.Body>
    </Container>
  );
}
