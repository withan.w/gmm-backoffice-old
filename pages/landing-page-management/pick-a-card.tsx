import React, { ReactElement, useState, useEffect } from "react";
import DashboardLayout from "../../src/components/Layout";
import Container from "../../src/components/Container";

interface Props {}

export default function PACManagement(props): ReactElement {
  return (
    <Container>
      <Container.Body>
        <DashboardLayout defaultSelectedKeys="2-6">
          <h3>Pick-A-Card Management</h3>
        </DashboardLayout>
      </Container.Body>
    </Container>
  );
}
