import React, { ReactElement, useState, useEffect } from "react";
import DashboardLayout from "../../src/components/Layout";
import Container from "../../src/components/Container";

interface Props {}

export default function MultiManagement(props): ReactElement {
  return (
    <Container>
      <Container.Body>
        <DashboardLayout defaultSelectedKeys="2-4">
          <h3>Multimedia Management</h3>
        </DashboardLayout>
      </Container.Body>
    </Container>
  );
}
