import { Form, Input, Button, message, Checkbox } from "antd";
import React, { ReactElement } from "react";
import {
  setStorage,
  _post,
  KEY_STORAGE,
  deleteStorage,
  getStorage,
} from "../src/utils";
import { useRouter } from "next/router";
import Container from "../src/components/Container";
import Footer from "../src/components/Footer";
import { Context } from "../src/lib/context";

interface Submit {
  email: string;
  password: string;
  remember: boolean;
}

export default function Login(): ReactElement {
  const { state, dispatch } = React.useContext(Context);
  const router = useRouter();
  const [initForm] = Form.useForm<Submit>();

  const onFinish = async (values: Submit) => {
    if (values.remember) {
      setStorage(KEY_STORAGE.remember, JSON.stringify(values.remember));
      setStorage(KEY_STORAGE.email, values.email);
    } else {
      deleteStorage(KEY_STORAGE.remember);
      deleteStorage(KEY_STORAGE.email);
    }
    await login(values);
  };

  const login = async ({ email, password }: Submit) => {
    try {
      const path = "/api/internal/login";
      const body = {
        email,
        password,
      };
      const res = await _post(path, body);
      if (res.result) {
        setStorage(KEY_STORAGE.token, res.data.access_token);
        dispatch({
          type: "SET_TOKEN",
          payload: {
            token: res.data.access_token,
          },
        });

        message.success("Login successfully");
        router.push(`/dashboard`);
      } else {
        message.warning("Please try again.");
      }
    } catch (error) {
      message.warning("Can not connect server.");
      console.log("*==ERROR WHILE LOGIN==*");
    }
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };

  React.useEffect(() => {
    const remember = getStorage(KEY_STORAGE.remember);
    if (remember) {
      const email = getStorage(KEY_STORAGE.email);
      if (email) {
        initForm.setFieldsValue({
          email,
          remember,
          password: "",
        });
      }
    } else {
      initForm.setFieldsValue({
        email: "",
        remember: false,
        password: "",
      });
    }
    if (state.error_message) {
      setTimeout(() => {
        message.warning(state.error_message, 8);
        dispatch({
          type: "SET_ERROR_MESSAGE",
          payload: {
            error_message: null,
          },
        });
      }, 2000);
    }
    return () => {};
  }, [router.query]);

  return (
    <Container>
      <Container.Body>
        <div
          style={{
            background: "#144781",
            display: "flex",
            justifyContent: "center",
            height: "100vh",
            flexDirection: "column",
            textAlign: "center",
            alignItems: "center",
          }}
        >
          <img src="/gmm_logo_white.png" width="230" alt="Logo GMM" />
          <div
            style={{
              padding: "16px 40px 16px 40px",
              background: "white",
              borderRadius: 4,
            }}
          >
            <div
              style={{
                fontSize: 18,
                fontWeight: "bold",
                padding: "32px 0px 32px 0px",
              }}
            >
              Login to Management System
            </div>

            <Form
              form={initForm}
              name="basic"
              labelCol={{ span: 8 }}
              wrapperCol={{ span: 32 }}
              layout="vertical"
              initialValues={{ remember: true }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Form.Item
                label="Email"
                name="email"
                rules={[
                  {
                    required: true,
                    type: "email",
                    message: "Please input your email",
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                label="Password"
                name="password"
                rules={[
                  { required: true, message: "Please input your password!" },
                ]}
              >
                <Input.Password />
              </Form.Item>

              <Form.Item
                name="remember"
                valuePropName="checked"
                style={{ textAlign: "left" }}
              >
                <Checkbox>Remember me</Checkbox>
              </Form.Item>

              <Form.Item>
                <Button type="primary" block htmlType="submit">
                  Login
                </Button>
              </Form.Item>
            </Form>
          </div>
        </div>
      </Container.Body>
      <Footer></Footer>
    </Container>
  );
}
