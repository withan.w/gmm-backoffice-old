import React, { ReactElement, useState } from "react";
import DashboardLayout from "../../src/components/Layout";
import Container from "../../src/components/Container";
import {
  Avatar,
  Button,
  Card,
  Form,
  Input,
  List,
  message,
  PageHeader,
  Select,
  Switch,
  Tag,
} from "antd";
import { theme } from "../../src/constants";

const { Option } = Select;

import { UserOutlined, UserAddOutlined } from "@ant-design/icons";
import { Context } from "../../src/lib/context";

interface Props {}

interface User {
  id: number;
  fullname: string;
  email: string;
  status: boolean;
  role: string;
}

export default function UserManagement(props): ReactElement {
  const [showRegisHideList, setShowRegisHideList] = useState(false);

  const { state } = React.useContext(Context);
  console.log("USER MANGEMENT PAGE");
  console.log("state : ", state);

  const togglePage = () => {
    setShowRegisHideList(!showRegisHideList);
  };

  const ListSection = () => {
    const onRoleChanged = (val: string, id: number) => {
      const data = {
        assign_to_id: id,
        change_role_to: val,
      };

      message.info("Role Changed!");
      console.log(data);
    };

    const onSwitchToggle = (val: boolean, id: number) => {
      const data = {
        assign_to_id: id,
        change_status_to: val,
      };

      message.warning("Wait to do!");
      console.log(data);
    };

    const paginationProps = {
      pageSize: 10,
    };

    const userList: User[] = [
      {
        id: 1,
        fullname: "Phatthakarn Jirayusakul (x10)",
        email: "phatthakarn.j@allrightbox.com",
        status: true,
        role: "ADMIN",
      },
      {
        id: 2,
        fullname: "Withan Wongsabut (x10)",
        email: "withan.w@allrightbox.com",
        status: true,
        role: "ADMIN",
      },
      {
        id: 3,
        fullname: "Netrchanok Chumsaengsi (x10)",
        email: "netrchanok.c@allrightbox.com",
        status: false,
        role: "ADMIN",
      },
      {
        id: 4,
        fullname: "Mock User 01",
        email: "user01@mock-user.com",
        status: false,
        role: "GUEST",
      },
      {
        id: 5,
        fullname: "Mock User 02",
        email: "user02@mock-user.com",
        status: true,
        role: "GUEST",
      },
      {
        id: 6,
        fullname: "Mock User 03",
        email: "user03@mock-user.com",
        status: true,
        role: "GUEST",
      },
      {
        id: 7,
        fullname: "Mock User 04",
        email: "user04@mock-user.com",
        status: true,
        role: "GUEST",
      },
      {
        id: 8,
        fullname: "Mock User 05",
        email: "user05@mock-user.com",
        status: true,
        role: "GUEST",
      },
      {
        id: 9,
        fullname: "Mock User 06",
        email: "user06@mock-user.com",
        status: true,
        role: "GUEST",
      },
      {
        id: 10,
        fullname: "Mock User 07",
        email: "user07@mock-user.com",
        status: true,
        role: "GUEST",
      },
      {
        id: 11,
        fullname: "Mock User 08",
        email: "user08@mock-user.com",
        status: true,
        role: "GUEST",
      },
      {
        id: 12,
        fullname: "Mock User 09",
        email: "user09@mock-user.com",
        status: true,
        role: "GUEST",
      },
      {
        id: 13,
        fullname: "Mock User 10",
        email: "user10@mock-user.com",
        status: true,
        role: "GUEST",
      },
      {
        id: 14,
        fullname: "Mock User 11",
        email: "user11@mock-user.com",
        status: true,
        role: "GUEST",
      },
      {
        id: 15,
        fullname: "Mock User 12",
        email: "user12@mock-user.com",
        status: true,
        role: "GUEST",
      },
    ];

    return (
      <>
        <PageHeader
          style={{
            position: "sticky",
            top: 63,
            background: "white",
            zIndex: 5,
            borderBottom: "1px solid #f2f2f2",
          }}
          ghost={true}
          title="User Management"
          extra={[
            <Button
              key="1"
              type="primary"
              icon={<UserAddOutlined />}
              onClick={togglePage}
            >
              Create User
            </Button>,
          ]}
        />

        <Card bordered={false} bodyStyle={{ padding: "16px 24px 16px 24px" }}>
          <List
            rowKey="id"
            itemLayout="horizontal"
            pagination={paginationProps}
            dataSource={userList}
            renderItem={(item) => (
              <List.Item
                key={item.id}
                actions={[
                  item.status ? (
                    <Tag color="success">Active</Tag>
                  ) : (
                    <Tag color="error">Block</Tag>
                  ),

                  <Switch
                    key={`switch-${item.id}`}
                    checked={item.status}
                    onChange={(val) => onSwitchToggle(val, item.id)}
                  />,
                  <Select
                    key={`select-${item.id}`}
                    defaultValue={item.role}
                    style={{ width: 120 }}
                    onChange={(val) => onRoleChanged(val, item.id)}
                  >
                    <Option value="ADMIN">Admin</Option>
                    <Option value="GUEST">Guest</Option>
                  </Select>,
                  <a
                    style={{ color: theme.error }}
                    key={`delete-${item.id}`}
                    onClick={(e) => {
                      e.preventDefault();
                      console.log(item);
                    }}
                  >
                    DELETE
                  </a>,
                ]}
              >
                <List.Item.Meta
                  avatar={
                    <Avatar
                      style={{
                        backgroundColor: "#829dbb",
                      }}
                      size="large"
                      icon={<UserOutlined />}
                    />
                  }
                  title={item.email}
                  description={item.role}
                />
              </List.Item>
            )}
          />
        </Card>
      </>
    );
  };

  const RegisSection = () => {
    const onFinish = (values: any) => {
      message.success("User Created!");
      togglePage();
    };

    return (
      <>
        <PageHeader
          style={{
            position: "sticky",
            top: 63,
            background: "white",
            zIndex: 5,
            borderBottom: "1px solid #f2f2f2",
          }}
          ghost={true}
          onBack={() => togglePage()}
          title="Create User"
        />

        <Card bordered={false} bodyStyle={{ padding: "16px 24px 16px 24px" }}>
          <Form
            layout="vertical"
            name="basic"
            onFinish={onFinish}
            autoComplete="off"
          >
            <Form.Item
              label="Full name"
              name="fullname"
              rules={[{ required: true, message: "Please input fullname" }]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Email"
              name="email"
              rules={[
                {
                  required: true,
                  message: "Please input email",
                  type: "email",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Choose a role permission"
              name="role"
              rules={[
                { required: true, message: "Please input your password!" },
              ]}
            >
              <Select>
                <Option value="ADMIN">Admin</Option>
                <Option value="GUEST">Guest</Option>
              </Select>
            </Form.Item>

            <Form.Item>
              <Button type="primary" htmlType="submit">
                Create
              </Button>
            </Form.Item>
          </Form>
        </Card>
      </>
    );
  };

  return (
    <Container>
      <Container.Body>
        <DashboardLayout defaultSelectedKeys="4">
          {showRegisHideList ? <RegisSection /> : <ListSection />}
        </DashboardLayout>
      </Container.Body>
    </Container>
  );
}
