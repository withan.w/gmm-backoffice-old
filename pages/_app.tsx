import type { AppProps } from "next/app";
import "antd/dist/antd.css";
import "../public/styles/global.css";

import { theme } from "../src/constants";
import { ConfigProvider } from "antd";
import React, { useState } from "react";
import Loader from "../src/components/Loader";
import delay from "../src/utils/delay";
import { useRouter } from "next/router";
import { reducer } from "../src/lib/reducers";
import { initialState, Context } from "../src/lib/context";
import { getStorage, KEY_STORAGE, setStorage } from "../src/utils";

ConfigProvider.config({
  theme: {
    primaryColor: theme.primary,
  },
});

function MyApp({ Component, pageProps }: AppProps) {
  const router = useRouter();
  const { query } = router;
  const [pageLoading, setPageLoading] = useState<boolean>(true);

  const [state, dispatch] = React.useReducer(reducer, initialState);
  const value = { state, dispatch };

  const setToken = (token: string) => {
    dispatch({
      type: "SET_TOKEN",
      payload: {
        token,
      },
    });
  };

  React.useEffect(() => {
    const token = getStorage(KEY_STORAGE.token);
    if (token) {
      setToken(token);
    }
    setTimeout(() => {
      setPageLoading(false);
    }, 2200);
    return () => {};
  }, [query]);

  return (
    <ConfigProvider>
      <Context.Provider value={value}>
        {pageLoading ? <Loader /> : <Component {...pageProps} />}
      </Context.Provider>
    </ConfigProvider>
  );
}

export default MyApp;
