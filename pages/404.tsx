import { Button, Result } from "antd";
import Link from "next/link";
import React, { ReactElement } from "react";
interface Props {}

function NotFound({}: Props): ReactElement {
  return (
    <div
      style={{
        height: "100vh",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Result
        status="404"
        title="404"
        subTitle="Sorry, the page you visited does not exist."
        extra={
          <Link href="/">
            <Button type="primary">Back Home</Button>
          </Link>
        }
      />
    </div>
  );
}

export default NotFound;
