import React, { ReactElement, useState } from "react";
import DashboardLayout from "../../src/components/Layout";
import Container from "../../src/components/Container";
import { PageHeader } from "antd";
import { useRouter } from "next/router";
import { theme } from "../../src/constants";

interface Props {}

export default function AddRole(props): ReactElement {
  const router = useRouter();

  return (
    <Container>
      <Container.Body>
        <DashboardLayout defaultSelectedKeys="5">
          <PageHeader
            style={{
              position: "sticky",
              top: 63,
              background: "white",
              zIndex: 5,
              borderBottom: "1px solid #f2f2f2",
            }}
            ghost={true}
            title="New Role"
            onBack={() => router.push("/role-management")}
          />

          <>
            <h1>*** FORM ADD ***</h1>
          </>
        </DashboardLayout>
      </Container.Body>
    </Container>
  );
}
