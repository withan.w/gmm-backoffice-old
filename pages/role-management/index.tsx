import React, { ReactElement, useState } from "react";
import DashboardLayout from "../../src/components/Layout";
import Container from "../../src/components/Container";
import { Button, PageHeader, Space, Table } from "antd";
import { useRouter } from "next/router";
import { theme } from "../../src/constants";
import { Context } from "../../src/lib/context";

import { PlusOutlined } from "@ant-design/icons";

interface Props {}

export default function RoleManagement(props): ReactElement {
  const router = useRouter();
  const { state } = React.useContext(Context);
  console.log("ROLE MANAGEMENT PAGE");
  console.log("state : ", state);

  const columns = [
    {
      title: "Role",
      dataIndex: "role",
      key: "role",
    },
    {
      title: "Description",
      dataIndex: "description",
      key: "description",
    },

    {
      title: "Number of users",
      dataIndex: "number_of_user",
      key: "number_of_user",
    },
    {
      title: "Action",
      key: "action",
      render: (text, record) => (
        <Space size="middle">
          <a onClick={() => router.push(`/role-management/edit/${record.key}`)}>
            Edit
          </a>
          <a>Delete</a>
        </Space>
      ),
    },
  ];

  const data = [
    {
      key: "1",
      role: "Admin",
      number_of_user: 1,
      description: "Allows to manage all systems",
    },
    {
      key: "2",
      role: "Guest",
      number_of_user: 1,
      description: "View all information but can't manage",
    },
    {
      key: "3",
      role: "Custom Role",
      number_of_user: 0,
      description: "Description",
    },
  ];

  return (
    <Container>
      <Container.Body>
        <DashboardLayout defaultSelectedKeys="5">
          <PageHeader
            style={{
              position: "sticky",
              top: 63,
              background: "white",
              zIndex: 5,
              borderBottom: "1px solid #f2f2f2",
            }}
            ghost={true}
            title="Role Management"
            extra={[
              <Button
                key="1"
                type="primary"
                icon={<PlusOutlined />}
                onClick={() => router.push("/role-management/add")}
              >
                New Role
              </Button>,
            ]}
          />

          <Table columns={columns} dataSource={data} />
        </DashboardLayout>
      </Container.Body>
    </Container>
  );
}
