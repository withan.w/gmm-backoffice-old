import React, { ReactElement, useState } from "react";
import DashboardLayout from "../../../src/components/Layout";
import Container from "../../../src/components/Container";
import { PageHeader } from "antd";
import { useRouter } from "next/router";
import { theme } from "../../../src/constants";

interface Props {}

export default function EditRole(props): ReactElement {
  const { id } = props;
  const router = useRouter();

  return (
    <Container>
      <Container.Body>
        <DashboardLayout defaultSelectedKeys="5">
          <PageHeader
            style={{
              position: "sticky",
              top: 63,
              background: "white",
              zIndex: 5,
              borderBottom: "1px solid #f2f2f2",
            }}
            ghost={true}
            title="Edit Role"
            onBack={() => router.push("/role-management")}
          />

          <>
            <h1>*** FORM EDIT ID : {id} ***</h1>
          </>
        </DashboardLayout>
      </Container.Body>
    </Container>
  );
}

export async function getServerSideProps({ params }) {
  return {
    props: { id: params.id },
  };
}
