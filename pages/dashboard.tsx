import React, { ReactElement, useState, useEffect } from "react";

import Container from "../src/components/Container";
import DashboardLayout from "../src/components/Layout";
import Guards from "../src/guards/Guard";
import { Context } from "../src/lib/context";

interface Props {}

export default function Dashboard(props): ReactElement {
  const { state } = React.useContext(Context);
  console.log("DASHBOARD PAGE");
  console.log("state : ", state);

  return (
    <Guards>
      <Container>
        <Container.Body>
          <DashboardLayout defaultSelectedKeys="1">
            <div>Dashboard</div>
          </DashboardLayout>
        </Container.Body>
      </Container>
    </Guards>
  );
}
