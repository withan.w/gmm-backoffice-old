import { ENV } from "../../../src/env/env";
import { NextApiRequest, NextApiResponse } from "next";
import { IFromResponses } from "../../../src/interface";

type reqData = {
  username: string;
  password: string;
};

const mockLoginSuccess = true; //<-- Toggle Mock

interface ExtendedNextApiRequest extends NextApiRequest {
  body: {
    email: string;
    password: string;
  };
}

export default async (
  req: ExtendedNextApiRequest,
  res: NextApiResponse<IFromResponses>
) => {
  if (req.method === "POST") {
    const { email, password } = req.body;

    try {
      if (mockLoginSuccess) {
        return res.json({
          result: true,
          status: 200,
          message: "Login success",
          data: {
            access_token:
              "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjAwMSIsIm5hbWUiOiJXaXRoYW4gIFdvbmdzYWJ1dCIsInJvbGUiOiIgU1VQRVJfQURNSU4iLCJibG9jayI6ZmFsc2UsInBlcm1pc3Npb24iOnsiZGFzaGJvYXJkIjp0cnVlLCJhb2NfbW5nIjp7InZpZXciOnRydWUsImVkaXQiOnRydWV9LCJ3YXBfbW5nIjp7InZpZXciOnRydWUsImVkaXQiOnRydWV9LCJvcGVyX21uZyI6eyJ2aWV3Ijp0cnVlLCJlZGl0Ijp0cnVlfSwibXVsdGlfbW5nIjp7InZpZXciOnRydWUsImVkaXQiOnRydWV9LCJxdWl6X21uZyI6eyJ2aWV3Ijp0cnVlLCJlZGl0Ijp0cnVlfSwicGFjX21uZyI6eyJ2aWV3Ijp0cnVlLCJlZGl0Ijp0cnVlfX19.GsO0AEhEHwUtXMdksmTSCKhS-z7DbvhQgnW45euopsY",
          },
        });
      } else {
        return res.json({
          result: false,
          status: 404,
          message: "Login fail",
        });
      }
    } catch (e) {
      return res.json({
        result: false,
        status: 500,
        message: "Internal Server Error",
      });
    }
  } else {
    return res.json({
      result: false,
      status: 405,
      message: "Method not allow",
    });
  }
};
