import React from "react";
import { Children } from "../interface";
import { _get, _post } from "../utils";
import { useRouter } from "next/router";
import { Context } from "../lib/context";
import { deleteAllStorage, deleteStorage, KEY_STORAGE } from "../utils/storage";

interface Props extends Children {}

export default function Guards({ children }: Props) {
  const router = useRouter();
  const { state, dispatch } = React.useContext(Context);

  const profile = async () => {
    try {
      const path = "/api/internal/get-profile";
      const body = { token: state.token };
      const res = await _post(path, body);

      if (res.result) {
        dispatch({
          type: "SET_ACCOUNT",
          payload: {
            account: res.data,
          },
        });
      } else {
        deleteStorage(KEY_STORAGE.token);
        router.push("/login");
      }
    } catch (error) {}
  };

  React.useEffect(() => {
    profile();
    return () => {};
  }, [state.token]);

  return <>{children}</>;
}
