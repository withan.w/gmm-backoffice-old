import axios from "axios";
import { IFromResponses } from "../interface";

const _get = async (url: string, config?: any): Promise<any> => {
  return new Promise<IFromResponses>((resolve) => {
    axios.get(url, { proxy: false }).then(
      (result) => {
        resolve(result.data);
      },
      (error) => {
        resolve(reTurnError(error));
      }
    );
  });
};

const _post = async (url: string, body?: any, config?: any): Promise<any> => {
  return new Promise<IFromResponses>((resolve) => {
    axios.post(url, body, { proxy: false }).then(
      (result) => {
        resolve(result.data);
      },
      (error) => {
        resolve(reTurnError(error));
      }
    );
  });
};

const reTurnError = (error) => {
  if (error?.response?.data?.status === 401) {
    const errJwt = {
      data: "",
      message: "กรุณา Login เข้าสู่ระบบใหม่อีกครั้ง",
      result: false,
      status: 401,
    };
    return errJwt;
  } else if (error?.response?.data) {
    return error.response.data;
  } else {
    return error;
  }
};

export { _get, _post };
