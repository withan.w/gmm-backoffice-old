import { CSSProperties } from "react";

export interface Children {
  children?: string | JSX.Element | (string | JSX.Element)[];
}
export interface Style {
  style?: CSSProperties | undefined;
}

export interface IFromResponses {
  result: boolean;
  status: number;
  message: string;
  data?: any;
}

export interface Account {
  id: string;
  email: string;
  role: string;
  permission: Permission;
}

interface Permission {
  dashboard_view: boolean;
  aoc_view: boolean;
  aoc_edit: boolean;
  wap_view: boolean;
  wap_edit: boolean;
  oper_view: boolean;
  oper_edit: boolean;
  multi_view: boolean;
  multi_edit: boolean;
  quiz_view: boolean;
  quiz_edit: boolean;
  pac_view: boolean;
  pac_edit: boolean;
}
