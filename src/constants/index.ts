export const theme = {
  primary: "#144781",
  secondary: "#D8D2CB",
  textLight: "#fff",
  textDark: "#171924",
  success: "#3ac462",
  warning: "#f3bf11",
  error: "#eb445a",
};
