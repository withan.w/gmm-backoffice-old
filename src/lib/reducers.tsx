import { deleteStorage, getStorage, KEY_STORAGE, setStorage } from "../utils";
import { Action, initialState, InitialState } from "./context";

export const reducer = (
  state: InitialState,
  { type, payload }: Action
): InitialState => {
  switch (type) {
    case "SET_INITIAL":
      return initialState;
    case "SET_ACCOUNT":
      return {
        ...state,
        account: payload.account,
      };
    case "SET_ERROR_MESSAGE":
      return {
        ...state,
        error_message: payload.error_message,
      };
    case "SET_TOKEN":
      return {
        ...state,
        token: payload.token,
      };
    case "LOGOUT":
      deleteStorage(KEY_STORAGE.token);
      return initialState;
    default:
      return state;
  }
};
