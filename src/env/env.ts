export const ENV = {
  ENV: process.env.ENV,
  APP_NAME: process.env.APP_NAME,
  VERSION: process.env.VERSION,
  DATABASE_URL: process.env.DATABASE_URL,
  BASE_URL: process.env.BASE_URL,
};
