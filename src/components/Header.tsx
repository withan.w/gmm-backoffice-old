import React from "react";
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
  LogoutOutlined,
} from "@ant-design/icons";

import { theme } from "../constants/index";
import { Button, message } from "antd";
import { useRouter } from "next/router";
import { deleteStorage, KEY_STORAGE } from "../utils";
import { Context } from "../lib/context";

type Props = {};

function Header({ menuCollapsed, setMenuCollapsed }) {
  const router = useRouter();
  const { state, dispatch } = React.useContext(Context);

  const toggleMenuCollapse = () => {
    setMenuCollapsed(!menuCollapsed);
  };

  const logout = () => {
    deleteStorage(KEY_STORAGE.token);
    dispatch({
      type: "SET_TOKEN",
      payload: {
        token: null,
      },
    });
    message.success("Log out successfully");
  };

  return (
    <div
      style={{
        padding: "0 24px",

        background: "#fff",
        position: "sticky",
        top: 0,
        height: 63,
        borderBottom: "1px solid #f2f2f2",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        zIndex: 5,
      }}
    >
      {menuCollapsed ? (
        <MenuUnfoldOutlined
          style={{ cursor: "pointer", fontSize: 16 }}
          onClick={toggleMenuCollapse}
        />
      ) : (
        <MenuFoldOutlined
          style={{ cursor: "pointer", fontSize: 16 }}
          onClick={toggleMenuCollapse}
        />
      )}

      <span style={{ flex: 1 }}></span>

      <div style={{ cursor: "pointer", textAlign: "end", paddingRight: 16 }}>
        <div
          style={{
            fontSize: 14,
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <UserOutlined style={{ fontSize: 16 }} />
          <span style={{ paddingLeft: 8 }}>{`Withan Wongsabut`}</span>
        </div>{" "}
        <div style={{ fontSize: 9, color: theme.primary }}>
          <span> {"SUPER ADMIN".toLocaleUpperCase()}</span>
        </div>
      </div>

      <Button onClick={logout} shape="circle" icon={<LogoutOutlined />} />
    </div>
  );
}

export default Header;
