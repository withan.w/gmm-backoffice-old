import React, { ReactElement, useState } from "react";
import { Layout, Menu, Spin } from "antd";
import {
  SettingOutlined,
  DashboardOutlined,
  ShakeOutlined,
  UsergroupAddOutlined,
  ProfileOutlined,
} from "@ant-design/icons";
import { useRouter } from "next/router";
import { Children } from "../interface";

import { theme } from "../constants/index";
import Header from "./Header";
const { SubMenu } = Menu;
const { Content, Sider, Footer } = Layout;

interface Props extends Children {
  defaultSelectedKeys: string;
}

const menus = [
  {
    id: "1",
    title: "Dashboard",
    disabled: false,
    path: "/dashboard",
    icon: <DashboardOutlined />,
    children: [],
  },
  {
    id: "2",
    title: "Landing Page",
    disabled: false,
    path: "/",
    icon: <SettingOutlined />,
    children: [
      {
        id: "2-1",
        title: "AOC",
        disabled: false,
        path: "/landing-page-management/aoc",
        icon: <ShakeOutlined />,
        children: [],
      },
      {
        id: "2-2",
        title: "WAP",
        disabled: false,
        path: "/landing-page-management/wap",
        icon: <ShakeOutlined />,
        children: [],
      },
      {
        id: "2-3",
        title: "OPER",
        disabled: false,
        path: "/landing-page-management/oper",
        icon: <ShakeOutlined />,
        children: [],
      },
      {
        id: "2-4",
        title: "Multimedia",
        disabled: false,
        path: "/landing-page-management/multimedia",
        icon: <ShakeOutlined />,
        children: [],
      },
      {
        id: "2-5",
        title: "Quiz",
        disabled: false,
        path: "/landing-page-management/quiz",
        icon: <ShakeOutlined />,
        children: [],
      },
      {
        id: "2-6",
        title: "Pick-A-Card",
        disabled: false,
        path: "/landing-page-management/pick-a-card",
        icon: <ShakeOutlined />,
        children: [],
      },
    ],
  },
  {
    id: "3",
    title: "Setting",
    disabled: false,
    path: "/setting",
    icon: <SettingOutlined />,
    children: [],
  },
  {
    id: "4",
    title: "User Management",
    disabled: false,
    path: "/user-management",
    icon: <UsergroupAddOutlined />,
    children: [],
  },
  {
    id: "5",
    title: "Role Management",
    disabled: false,
    path: "/role-management",
    icon: <ProfileOutlined />,
    children: [],
  },
];

export default function DashboardLayout({
  children,
  defaultSelectedKeys,
}): ReactElement {
  console.log("=LAYOUT RENDER=");
  const router = useRouter();

  const [loading, setloading] = useState(false);
  const [menuCollapsed, setMenuCollapsed] = useState(false);

  const itemClick = (menu: any) => {
    if (router.pathname !== menu.path) {
      setloading(true);
      setTimeout(() => {
        router.push(menu.path);
      }, 300);
    }
  };

  return (
    <div>
      <Layout hasSider>
        <Sider
          trigger={null}
          collapsible
          collapsed={menuCollapsed}
          width={240}
          style={{
            borderRight: "1px solid #f2f2f2",
            backgroundColor: theme.primary,
            overflow: "hidden",
            height: "100vh",
            position: "sticky",
            left: 0,
            top: 0,
            bottom: 0,
          }}
        >
          <div className="ly-logo">
            {!menuCollapsed && (
              <img src="/gmm_logo_white.png" width="80%" alt="Logo GMM" />
            )}
            {menuCollapsed && (
              <img src="/gmm_logo_white.png" alt="Icon GMM" width={45} />
            )}
          </div>
          <Menu
            mode="inline"
            defaultSelectedKeys={[defaultSelectedKeys]}
            defaultOpenKeys={["2", "3"]}
            style={{
              borderRight: 0,
            }}
          >
            {menus.map((item) => {
              if (item.children.length) {
                return (
                  <SubMenu key={item.id} icon={item.icon} title={item.title}>
                    {item.children.map((c) => (
                      <Menu.Item
                        key={c.id}
                        icon={c.icon}
                        onClick={() => itemClick(c)}
                      >
                        {c.title}
                      </Menu.Item>
                    ))}
                  </SubMenu>
                );
              } else {
                return (
                  <Menu.Item
                    key={item.id}
                    icon={item.icon}
                    onClick={() => itemClick(item)}
                  >
                    {item.title}
                  </Menu.Item>
                );
              }
            })}
          </Menu>
        </Sider>

        <Layout className="site-layout">
          <Header
            menuCollapsed={menuCollapsed}
            setMenuCollapsed={setMenuCollapsed}
          />

          <Content
            className="site-layout-background"
            style={{
              margin: 0,
              padding: 16,
            }}
          >
            <Spin
              style={{ display: "flex", height: "100vh" }}
              tip="Loading..."
              spinning={loading}
            >
              {children}
            </Spin>
          </Content>
          <Footer style={{ textAlign: "center", backgroundColor: "white" }}>
            <span>GMM Landing Page System</span>
            <br />
            <span style={{ fontSize: 9 }}>v0.1.0</span>
          </Footer>
        </Layout>
      </Layout>
    </div>
  );
}
