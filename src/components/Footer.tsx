import React from "react";

type Props = {};

export default function Footer({}: Props) {
  return (
    <footer
      style={{
        color: "white",
        padding: 16,
        height: 70,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        background: "#144781",
      }}
    >
      <div style={{ textAlign: "center", fontSize: 12 }}>
        <span>
          <strong>GMM Landing Page System</strong>
        </span>
        <br />
        <span style={{ fontSize: 9 }}>v0.1.0</span>
      </div>
    </footer>
  );
}
