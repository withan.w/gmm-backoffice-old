import { Spin } from "antd";
import React from "react";

type Props = {};

const Loader = (props: Props) => {
  return (
    <div
      style={{
        display: "flex",
        height: "100vh",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Spin size="large" tip="Loading..."></Spin>
    </div>
  );
};

export default Loader;
